from flask import Flask, render_template, url_for, request, redirect, make_response, flash, session
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import secure_filename
from datetime import datetime as dt
import os

app = Flask(__name__)
app.config['SECRET_KEY'] = 'khdfnviuh1jdhf4'

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['UPLOAD_FOLDER'] = 'static\images'
ALLOWED_EXTENSIONS = {'jpg', 'png', 'jpeg'}
albums = SQLAlchemy(app)
users = SQLAlchemy(app)


class Album(albums.Model):
    prop = ['title', 'release_date', 'information']
    id = albums.Column(albums.Integer, primary_key=True)
    title = albums.Column(albums.String(100), nullable=False)
    release_date = albums.Column(albums.String(10), nullable=False)
    picture_path = albums.Column(albums.String(100), nullable=False)
    information = albums.Column(albums.Text, nullable=False)

    def __repr__(self):
        return self


class User(users.Model):
    id = users.Column(users.Integer, primary_key=True)
    login = users.Column(users.String(100), nullable=False)
    password = users.Column(users.String(100), nullable=False)
    privilege = users.Column(users.Integer, nullable=True)

    def __repr__(self):
        return self


albums_list = Album.query.order_by(Album.release_date.desc()).all()

files = os.listdir('templates')
titles = {
    'main': 'Головна сторінка',
    'about': 'Про гурт',
    'history': 'Історія',
    'signup': 'Авторизація',
    'register': 'Реєстрація',
    'settings': 'Налаштування'
}


@app.route('/')
@app.route('/main')
def home():
    albums_list = Album.query.order_by(Album.release_date.desc()).all()
    if 'user' in session and session['user'] != '':
        us = User.query.filter_by(login=session['user']).first()
        return render_template('main.html',
                               title=titles['main'],
                               display='authorized',
                               alb=albums_list,
                               priv=us.privilege)
    else:
        return render_template('main.html',
                               title=titles['main'],
                               display='start',
                               alb=albums_list)


@app.route('/register', methods=['POST', 'GET'])
def register():
    albums_list = Album.query.order_by(Album.release_date.desc()).all()
    if ('user' in session and session['user'] != ''):
        us = User.query.filter_by(login=session['user']).first()
        return render_template('main.html',
                               title=titles['main'],
                               display='authorized',
                               priv=us.privilege)
    elif (request.method == 'POST'):
        user = [
            request.form['login'], request.form['password'],
            request.form['passwordCheck']
        ]
        logins = [i.login for i in User.query.all()]
        if (user[0] in logins):
            flash('Такий користувач вже існує!', category='error')
        else:
            if (user[1] == user[2]):
                new_user = User(login=user[0], password=user[1], privilege=0)
                try:
                    users.session.add(new_user)
                    users.session.commit()
                    flash('Користувача зареєстровано!', category='success')
                except:
                    flash('Помилка роботи бази даних!', category='error')
                    pass
            else:
                flash('Паролі не співпадають!', category='error')
    return render_template('main.html',
                           title=titles['register'],
                           register=True)


@app.route('/signup', methods=['POST', 'GET'])
def signup():
    albums_list = Album.query.order_by(Album.release_date.desc()).all()
    if ('user' in session and session['user'] != ''):
        return redirect(url_for('authorized'))
    elif (request.method == 'POST'):
        us = User.query.filter_by(login=request.form['login']).first()
        user = [request.form['login'], request.form['password']]
        try:
            if (user[0] == us.login and user[1] == us.password):
                session['user'] = request.form['login']
                return redirect(url_for('authorized'))
            else:
                flash('Неправильний логін/пароль!', category='error')
        except:
            flash('Користувач не існує!')

    return render_template('main.html', title='Авторизація', signup=True)


@app.route('/authorized')
def authorized():
    albums_list = Album.query.order_by(Album.release_date.desc()).all()
    us = User.query.filter_by(login=session['user']).first()
    return render_template('main.html',
                           title=titles['main'],
                           display='authorized',
                           alb=albums_list,
                           priv=us.privilege)


@app.route('/exit')
def exit():
    albums_list = Album.query.order_by(Album.release_date.desc()).all()
    session['user'] = ''
    return render_template('main.html',
                           title=titles['main'],
                           display='start',
                           alb=albums_list)


@app.route('/settings', methods=['POST', 'GET'])
def settings():
    if (request.method == 'POST'):
        l = len(request.form)
        keys = ['title', 'release_date', 'information']
        if (l == 1):
            alb = Album.query.get_or_404(request.form['album_select'])
            try:
                albums.session.delete(alb)
                albums.session.commit()
                return redirect(url_for('authorized'))
            except:
                flash('Помилка видалення!', category='error')
        elif (l == 3):
            if ('picture_path' in request.files):
                flash('Файл загрузився', 'success')
                file = request.files['picture_path']
                if (file.filename != '' and allowed_file(file.filename)):
                    filename = secure_filename(file.filename)
                    file.save(
                        os.path.join(app.config['UPLOAD_FOLDER'], filename))
                    date = request.form['release_date'].split('-')
                    new_alb = {
                        'title': request.form['title'],
                        'date': request.form['release_date'],
                        'pic': filename,
                        'info': request.form['information']
                    }
                    try:
                        alb = Album(title=new_alb['title'],
                                    release_date=new_alb['date'],
                                    picture_path=new_alb['pic'],
                                    information=new_alb['info'])
                        albums.session.add(alb)
                        albums.session.commit()
                    except:
                        flash('Помилка роботи бази даних!', category='error')
                    return redirect(url_for('authorized'))
                else:
                    flash('Відсутня ілюстрація!', category='error')
        elif (l == 4):
            fl = False
            alb = Album.query.get(request.form['album_select'])
            if ('picture_path' in request.files):
                file = request.files['picture_path']
                if (file.filename != '' and allowed_file(file.filename)):
                    filename = secure_filename(file.filename)
                    file.save(
                        os.path.join(app.config['UPLOAD_FOLDER'], filename))
                    alb.picture_path = filename
                    fl = True
            for i in range(3):
                if (request.form[alb.prop[i]] != ''):
                    setattr(alb, alb.prop[i], request.form[alb.prop[i]])
                    fl = True
            if (fl):
                albums.session.commit()
                return redirect(url_for('authorized'))
            else:
                flash('Альбом не змінено', category='error')

    us = User.query.filter_by(login=session['user']).first()
    if (us.privilege == 1):
        return render_template('main.html',
                               title=titles['settings'],
                               settings=True,
                               alb=albums_list)
    else:
        return redirect(url_for('authorized'))


def allowed_file(name):
    return '.' in name and \
        name.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/albums')
def albums_route():
    albums_list = Album.query.order_by(Album.release_date.desc()).all()
    if 'user' in session and session['user'] != '':
        us = User.query.filter_by(login=session['user']).first()
        return render_template('albums.html',
                               title=titles['main'],
                               display='authorized',
                               alb=albums_list,
                               priv=us.privilege)
    else:
        return render_template('albums.html',
                               title=titles['main'],
                               display='start',
                               alb=albums_list)


@app.route('/albums/<string:alb>')
def album(alb):
    us = User.query.filter_by(login=session['user']).first()
    alb = Album.query.filter_by(title=alb).first()
    if 'user' in session and session['user'] != '':
        return render_template('album_template.html',
                               title=alb.title,
                               display='authorized',
                               alb=albums_list,
                               info=alb,
                               priv=us.privilege)
    else:
        return render_template('album_template.html',
                               title=alb.title,
                               display='start',
                               alb=albums_list,
                               info=alb)


@app.route('/<string:name>', methods=['POST', 'GET'])
def render(name):
    us = User.query.filter_by(login=session['user']).first()
    if (f'{name}.html' in files):
        if 'user' in session and session['user'] != '':
            return render_template(name + '.html',
                                   title=titles[name],
                                   display='authorized',
                                   alb=albums_list,
                                   priv=us.privilege)
        else:
            return render_template(name + '.html',
                                   title=titles[name],
                                   display='start',
                                   alb=albums_list)

    else:
        return render_template('error.html')


@app.errorhandler(404)
def pageNotFound(error):
    return render_template('error.html')


if __name__ == "__main__":
    app.run()